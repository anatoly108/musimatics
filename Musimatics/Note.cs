﻿namespace Musimatics
{
    public class Note
    {
        public double X { get; private set; } 
        public double Y { get; private set; }

        public Note(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}