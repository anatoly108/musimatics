﻿using System;
using System.Collections.Generic;

namespace Musimatics
{
    public class Musician
    {
        private double _rhythm;
        private double _pause;

        public Musician(double rhythm, double pause)
        {
            _rhythm = rhythm;
            _pause = pause;
        }

        public void Play(IEnumerable<Note> sequence)
        {
            foreach (var note in sequence)
            {
                var frequency = Math.Abs((int)(note.Y));
                Console.Beep(frequency, 200);
            }
        }
    }
}