﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Musimatics
{
    class Program
    {
        static void Main(string[] args)
        {
//            var notes = new[] {-8, -6, -5, -3, -1, 0, 2};
//            var notes = new[] {0, 2, 4, 6, 7, 9, 11};
//            var notes = new[] {-14, -12, -10, -9, -7, -5, -4, -2, 0, 1, 3, 5, 6, 8, 10, 11, 13, 15, 16};
//            var notes = new[] {-4, -2, -1, 1, 3, 4, 6, 8};
            var notes = new[] {-1, 1, 2, 4, 6, 7, 9, 11};

            const int rhythm = 1;
            const int pause = 0;
            const double frequency = 1;
            var random = new Random();
            Mathematician.Func func = x =>
            {
                var octave = x/notes.Count();
                var note = random.Next(notes.Count());//(int) x%notes.Count();
                var i = notes[note];
                var i1 = (double)i / 12;
                return 440 * Math.Pow(2, i1);
            };
            var mathematician = new Mathematician(1, 1000);
            var sequence = mathematician.Solve(func, frequency);

            
            var musician = new Musician(rhythm, pause);

            /**
             * - Математик возвращает коллекцию типа Notation, а Музыкант её проигрывает
             * 
             * - Заюзать NAudio в Музыканте
             */

           musician.Play(sequence);
        }
    }
}
