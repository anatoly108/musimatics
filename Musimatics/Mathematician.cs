﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Musimatics
{
    public class Mathematician
    {
        public delegate double Func(double x);

        private readonly int _xFrom;
        private readonly int _xTo;

        public Mathematician(int xFrom, int xTo)
        {
            _xFrom = xFrom;
            _xTo = xTo;
        }

        public IEnumerable<Note> Solve(Func func, double frequency)
        {
            var count = (int) Math.Floor(_xTo/frequency);
            var results = new Note[count];

            double x = _xFrom;
            for (var i = 0; i < count; i++)
            {
                results[i] = new Note(x, func(x));
                x += frequency;
            }

            return results;
        }
    }
}